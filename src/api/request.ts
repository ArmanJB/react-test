import axios, { AxiosRequestConfig, AxiosResponse, Method } from "axios";

const api = axios.create({
  baseURL: "http://000.000.000.000:0000/api",
});

const request = <T>(
  method: Method,
  url: string,
  config: Omit<AxiosRequestConfig, "method" | "url">
): Promise<AxiosResponse<T>> => {
  return api.request<T>({
    method,
    url,
    ...config,
  });
};

export const post = <T, D>(
  url: string,
  data: D
): Promise<AxiosResponse<T, D>> =>
  request<T>("POST", url, {
    data,
    headers: { "Content-Type": "application/json" },
  });
