export interface LoginResponse extends User {
  accessToken: string;
  expiresIn: number;
}

export interface User {
  userId: string;
  userName: string;
  fullName: string;
  email: string;
  roles: string[];
}

export interface AppUser {
  user: User;
  accessToken: string;
  expiresIn: number;
}

export interface UserLogin {
  userName: string;
  password: string;
}
