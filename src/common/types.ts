import { LazyExoticComponent } from "react";

export type Page = LazyExoticComponent<() => JSX.Element>;
