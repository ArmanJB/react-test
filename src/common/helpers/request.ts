import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";

const axiosInst: AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
});

export const fetchData = async <R>(options: AxiosRequestConfig): Promise<R> =>
  await axiosInst(options)
    .then((response: AxiosResponse) => response.data)
    .catch((error: any) => {
      throw getErrorMessage(error);
    });

const getErrorMessage = (error: any): string => {
  let errorMsg: string = "Ha ocurrido un error inesperado";

  if (error && error.data) {
    errorMsg = error.data.error;
  }

  return errorMsg;
};
