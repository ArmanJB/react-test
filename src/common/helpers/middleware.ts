import { Dispatch } from "react";
import { AxiosRequestConfig, AxiosRequestHeaders } from "axios";

import { fetchData } from "./request";

export async function fetchPOST<D, A, R>(
  token: string | undefined,
  dispatch: Dispatch<A>,
  endpoint: string,
  body: D,
  objectResponseName: string | undefined,
  loadingAction: () => A,
  successCB: (response: R) => void,
  errorCB: (msg: string) => void
) {
  const headers: AxiosRequestHeaders = { "Content-Type": "application/json" };
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  const options: AxiosRequestConfig<D> = {
    url: endpoint,
    method: "POST",
    headers: headers,
    data: body,
  };

  fetchRequest<D, A, R>(
    options,
    objectResponseName,
    dispatch,
    loadingAction,
    successCB,
    errorCB
  );
}

async function fetchRequest<D, A, R>(
  options: AxiosRequestConfig<D>,
  objectResponseName: string | undefined,
  dispatch: Dispatch<A>,
  loadingAction: () => A,
  successCB: (response: R) => void,
  errorCB: (msg: string) => void
) {
  try {
    dispatch(loadingAction());
    const response: Awaited<R> = await fetchData<R>(options);

    // successCB(objectResponseName ? response[objectResponseName] : response);
  } catch (error: any) {
    if (error instanceof Error) {
      errorCB(error.message);
    } else {
      errorCB(error);
    }
  }
}
