import Cookies from "js-cookie";

import { AppUser } from "./interfaces";

export const getLoggedUser = (): AppUser | undefined => {
  const user = Cookies.get("user");
  return user
    ? typeof user === "object"
      ? user
      : JSON.parse(user)
    : undefined;
};

export const setSession = (user: AppUser | undefined): void => {
  if (user) {
    Cookies.set("user", JSON.stringify(user));
  } else {
    Cookies.remove("user");
  }
};
