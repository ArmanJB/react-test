import { Dispatch } from "react";

import { Action, createAction } from "../../common/context/actions";
import { AppUser } from "../../common/interfaces";

export enum AuthActionTypes {
  SIGN_IN = "AuthAction.SIGN_IN",
  LOG_OUT = "AuthAction.LOG_OUT",
}

type SignInAction = Action<typeof AuthActionTypes.SIGN_IN, AppUser>;
type LogOutAction = Action<typeof AuthActionTypes.LOG_OUT, undefined>;

export type AuthAction = SignInAction | LogOutAction;

export interface AuthActionProps {
  signIn: (user: AppUser) => void;
  logOut: () => void;
}

const signIn = (user: AppUser): SignInAction =>
  createAction(AuthActionTypes.SIGN_IN, user);

const logOut = (): LogOutAction => createAction(AuthActionTypes.LOG_OUT);

export const useAuthActions = (
  dispatch: Dispatch<AuthAction>
): AuthActionProps => ({
  signIn: (user: AppUser) => dispatch(signIn(user)),
  logOut: () => dispatch(logOut()),
});
