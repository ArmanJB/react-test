import { getLoggedUser } from "../../common/helpers";
import { AppUser } from "../../common/interfaces";
import { AuthAction, AuthActionTypes } from "./actions";

export interface AuthState {
  user: AppUser | undefined;
}

export const INIT_STATE: AuthState = {
  user: getLoggedUser(),
};

export const AuthReducer = (
  state: AuthState,
  action: AuthAction
): AuthState => {
  switch (action.type) {
    case AuthActionTypes.SIGN_IN:
      return { ...state, user: action.payload };
    case AuthActionTypes.LOG_OUT:
      return { ...state, user: undefined };
    default:
      return state;
  }
};
