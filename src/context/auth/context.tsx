import React, { createContext, useContext, useReducer } from "react";

import { AuthActionProps, useAuthActions } from "./actions";
import { AuthReducer, AuthState, INIT_STATE } from "./reducer";

interface AuthContextProps extends AuthActionProps {
  state: AuthState;
}

const AuthContext = createContext<AuthContextProps | undefined>(undefined);

export const AuthContextProvider = (props: any) => {
  const [authState, dispatch] = useReducer(AuthReducer, INIT_STATE);
  const actions = useAuthActions(dispatch);

  return (
    <AuthContext.Provider {...props} value={{ state: authState, ...actions }} />
  );
};

export function useAuthContext(): AuthContextProps {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error("Auth Provider not found");
  }

  return context;
}
