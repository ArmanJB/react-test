import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";

import { AuthContextProvider } from "./context/auth/context";
import { AppRoutes } from "./routes/AppRoutes";

// import logo from "./logo.svg";
import "./assets/css/App.css";

const queryClient = new QueryClient();

export default function App() {
  // return <h1 className="text-3xl   font-bold underline">Hello world!</h1>;
  return (
    <AuthContextProvider>
      <QueryClientProvider client={queryClient}>
        <AppRoutes />
      </QueryClientProvider>
    </AuthContextProvider>
  );
}
