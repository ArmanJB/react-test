import React from "react";
import Header from "../../components/Header";

interface Props {
  children: JSX.Element;
}

const MainLayout = ({ children }: Props) => {
  return (
    <div>
      <p>MainLayout</p>
      <Header />
      {children}
    </div>
  );
};

export default MainLayout;
