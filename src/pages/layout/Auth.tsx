import React from "react";

interface Props {
  children: JSX.Element;
}

const AuthLayout = ({ children }: Props) => {
  return (
    <div>
      <p>AuthLayout</p>
      {children}
    </div>
  );
};

export default AuthLayout;
