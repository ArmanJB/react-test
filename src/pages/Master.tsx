import React, { lazy, LazyExoticComponent, Suspense } from "react";

import { useAuthContext } from "../context/auth/context";
import { AppUser } from "../common/interfaces";
import { Page } from "../common/types";

type AppLayout = LazyExoticComponent<
  ({ children }: { children: JSX.Element }) => JSX.Element
>;

const MainLayout: AppLayout = lazy(() => import("./layout/Main"));
const AuthLayout: AppLayout = lazy(() => import("./layout/Auth"));

const Master = (WrappedComponent: Page) => {
  const { state } = useAuthContext();

  const getLayout = (user: AppUser | undefined) => {
    if (user) {
      return MainLayout;
    }

    return AuthLayout;
  };

  const Layout: AppLayout = getLayout(state.user);

  return (
    <Suspense fallback={<p>Loading...</p>}>
      <Layout>
        <WrappedComponent />
      </Layout>
    </Suspense>
  );
};

export default Master;
