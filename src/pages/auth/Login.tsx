import React, { useEffect } from "react";
import { useMutation } from "react-query";
import { useNavigate } from "react-router-dom";

import { useAuthContext } from "../../context/auth/context";
import { LoginResponse, UserLogin, AppUser } from "../../common/interfaces";
import { setSession } from "../../common/helpers";
import { post } from "../../api/request";

const Login = () => {
  const navigator = useNavigate();
  const { state, signIn } = useAuthContext();
  const { isLoading, mutate } = useMutation(
    async (variables: UserLogin) => {
      return await post<LoginResponse, UserLogin>("/Account/Auth", variables);
    },
    {
      onSuccess: (res) => {
        const appUsr: AppUser = {
          user: {
            userId: res.data.userId,
            userName: res.data.userName,
            fullName: res.data.fullName,
            email: res.data.email,
            roles: res.data.roles,
          },
          accessToken: res.data.accessToken,
          expiresIn: res.data.expiresIn,
        };

        setSession(appUsr);
        signIn(appUsr);
      },
      onError: (err) => {
        console.error(err);
      },
    }
  );

  const handleSubmit = (event: React.SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault();
    const form = event.currentTarget;
    const formElements = form.elements as typeof form.elements & {
      username: { value: string };
      password: { value: string };
    };

    mutate({
      userName: formElements.username.value,
      password: formElements.password.value,
    });
  };

  useEffect(() => {
    if (state.user) {
      navigator("/");
    }
  }, [state]);

  return (
    <>
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="username"
          placeholder="username"
          defaultValue="LR02141"
        />
        <input
          type="password"
          name="password"
          placeholder="password"
          defaultValue="Prueba123+"
        />
        <button type="submit">Enviar</button>
      </form>
    </>
  );
};

export default Login;
