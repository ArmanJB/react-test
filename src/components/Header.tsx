import React from "react";
import { setSession } from "../common/helpers";

import { useAuthContext } from "../context/auth/context";

const Header = () => {
  const { logOut } = useAuthContext();

  const handleLogout = () => {
    setSession(undefined);
    logOut();
  };

  return (
    <>
      <button type="button" onClick={handleLogout}>
        Logout
      </button>
    </>
  );
};

export default Header;
