import React, { ReactNode } from "react";
import { Navigate } from "react-router-dom";
import { useAuthContext } from "../context/auth/context";

const PrivateRoute = ({ component }: { component: ReactNode }): JSX.Element => {
  const { state } = useAuthContext();

  if (state.user) {
    return <>{component}</>;
  }

  return <Navigate to="/auth" />;
};

export default PrivateRoute;
