import React, { lazy } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { Page } from "../common/types";
import PrivateRoute from "../components/PrivateRoute";

import Master from "../pages/Master";
const Login: Page = lazy(() => import("../pages/auth/Login"));
const Dashboard: Page = lazy(() => import("../pages/application/Dashboard"));

export const AppRoutes = () => {
  const baseRoute: string | undefined = undefined;

  return (
    <BrowserRouter basename={baseRoute}>
      <Routes>
        <Route
          path="/"
          element={<PrivateRoute component={Master(Dashboard)} />}
        />
        <Route path="/auth" element={Master(Login)} />
      </Routes>
    </BrowserRouter>
  );
};
